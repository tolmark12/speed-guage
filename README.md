## Speed Guage
![Sample Guage](http://shots.delorum.com/client/view/Screen%20Shot%202015-05-20%20at%2011.58.28%20AM.png)

## Usage
```coffeescript
###
 Instantiate an instance of the guage for each of your metrics
 @parentElement : a jquery parent container.
 @metricName    : The Metric name
 @kind          : Whether this is "dedicated" or "shared" (not currently used)
###
testGuage = new pxguage.SpeedGuage( $('body'), "RAM", "dedicated" )

# Set the same thresholds for Global and Alloc
testGuage.setThresholds 0.7, 0.8

# Or set these thresholds separately
setAllocThresholds 0.7, 0.8
setUsageThresholds 0.7, 0.8

# Set percentage allocated
testGuage.setAlloc .69

# Set actual Usage
testGuage.setUsage .95

# Set Usage divided into sub categories
# Note : 0.019 is the smallest number that will show up as a dot
testGuage.setSubUsage [
  {name:"kernel",       val:.25}
  {name:"ZFS",          val:.2 }
  {name:"Global Zones", val:.15}
  {name:"User Zones",   val:.05}
]
```
