class SpeedGuage
  @count : 0

  constructor: ( $el, resourceKind, @kind ) ->
    @id = "guage#{SpeedGuage.count++}"
    @build $el, resourceKind

  # ------------------ API

  # Thresholds
  setThresholds      : (warmBorder=0.75, hotBorder=0.85) -> @setUsageThresholds(warmBorder, hotBorder); @setAllocThresholds(warmBorder, hotBorder)
  setUsageThresholds : (@warmUsageBorder=0.75, @hotUsageBorder=0.85) ->
  setAllocThresholds : (@warmAllocBorder=0.75, @hotAllocBorder=0.85) ->

  setAlloc : (perc) ->
    temperature = @getTemperature(perc, "Alloc")
    $alloc = $(".alloc", @$node)
    $alloc.attr "class", "alloc #{temperature}"
    $alloc.html Math.round(100*perc)
    @arch.drawAlloc perc, temperature

  setUsage : (perc) ->
    temperature = @getTemperature perc, "Usage"
    @displayGlobalUsage perc, temperature
    @arch.drawSingleUsage perc, temperature

  ###
  @subUsageAr : Array of object in the following format -> {name:"Kernel", usage:"24"}
  ###
  setSubUsage    : (subUsageAr) ->
    # Add Color to each item
    colors = pxguage.Colors.getSubColors()
    ind    = 0
    for item in subUsageAr
      item.color = colors[ind++]
      if ind == colors.length then ind = 0

    # add the items to the html
    node = jadeTemplate['sub-items']( { subItems:subUsageAr} )
    $subItems = $(".sub-items", @$node)
    $subItems.append( $(node) )

    # Calculate the total usage and display it
    totalUsage = 0
    totalUsage += item.val for item, i in subUsageAr
    @displayGlobalUsage totalUsage, @getTemperature( totalUsage, "Usage")
    @arch.drawMultiUsage subUsageAr


  # ------------------ PRIVATE

  build : ( $el, resourceKind ) ->
    node   = jadeTemplate['guage']( { title:resourceKind, id:@id  } )
    @$node = $(node)
    $el.append( @$node )
    @arch = new pxguage.Arch @id

  displayGlobalUsage : (perc, temperature) ->
    $used = $(".used",  @$node)
    $used.attr "class", "used #{temperature}"
    $used.html Math.round(100*perc)

  getTemperature : (perc, kind) ->
    if perc < @["warm#{kind}Border"]     then return "cool"
    else if perc < @["hot#{kind}Border"] then return "warm"
    else                                 return "hot"

pxguage = {}
pxguage.SpeedGuage = SpeedGuage
