class Colors

  @archBg    : "#131313"
  @warmColor : "#FBB869"
  @hotColor  : "#C12026"

  @getArchColors : () ->
    # bgArchColor, archColor, strokeColor
    [@archBg, "#000", "#2A3D54" ]

  @getWarmArchColors : () ->
    [@archBg, "#301C0F", @warmColor]

  @getHotArchColors : () ->
    [@archBg, "#351818", @hotColor]

  @getSubColors : () ->
    [
      "#DE4775"
      "#7FC34A"
      "#F9BE85"
      "#85D3E6"
    ]

pxguage.Colors = Colors
