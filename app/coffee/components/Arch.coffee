class Arch

  constructor: (id) ->
    @radius = 67
    @build id

  build : (id) ->
    @stage = new createjs.Stage id
    @stage.x = @stage.y = 12

  drawAlloc : (perc, temperature) ->
    if temperature == "cool"
      [ bgArchColor, archColor, strokeColor ] = pxguage.Colors.getArchColors()
    else if temperature == "warm"
      [ bgArchColor, archColor, strokeColor ] = pxguage.Colors.getWarmArchColors()
    else
      [ bgArchColor, archColor, strokeColor ] = pxguage.Colors.getHotArchColors()

    if @alloc? then @stage.removeChild @alloc

    g = new createjs.Graphics()
    @alloc = new createjs.Shape g
    @alloc.x = @radius
    @alloc.y = @radius
    endingAngle = (180 - 180 * perc) * -0.0174532925

    # Draw Full Background Guide Arch
    g.setStrokeStyle 16
    g.beginStroke bgArchColor
    g.arc 0,0, @radius, 180*0.0174532925, 0
    g.endStroke()

    # Draw Background Arch
    g.beginStroke archColor
    g.arc 0,0, @radius, 180*0.0174532925, endingAngle
    g.endStroke()

    # Draw Top Line
    g.setStrokeStyle 3.5
    g.beginStroke strokeColor
    g.arc 0,0, @radius+8, 180*0.0174532925, endingAngle
    g.endStroke()

    # DrawBottom Line
    g.beginStroke strokeColor
    g.arc 0,0, @radius-8, 180*0.0174532925, endingAngle

    @stage.addChild @alloc
    @stage.update()

  drawSingleUsage : (perc, temperature) ->
    if temperature == "cool"      then color = "white"
    else if temperature == "warm" then color = pxguage.Colors.warmColor
    else color = pxguage.Colors.hotColor

    if @usage? then @stage.removeChild @usage
    @usage = new createjs.Container()
    @stage.addChild @usage

    targAngle  = -180 + 180*perc
    curAngle   = -180
    while curAngle < targAngle
      curAngle += 3.4
      x1 = @radius + ( Math.cos(curAngle*(Math.PI/180)) * @radius )
      y1 = @radius + ( Math.sin(curAngle*(Math.PI/180)) * @radius )
      g = new createjs.Graphics()
      g.beginFill color
      g.drawCircle(x1, y1, 1.25)
      s = new createjs.Shape g
      @usage.addChild s
    @stage.update()

  drawMultiUsage : (ar) ->
    if @usage? then @stage.removeChild @usage
    @usage = new createjs.Container()
    @stage.addChild @usage

    curAngle   = -180
    g = new createjs.Graphics()
    s = new createjs.Shape g
    for item in ar
      targAngle = curAngle + 180*item.val
      while curAngle < targAngle-3.4
        @drawCirc g, curAngle, 1.25, item.color
        isFirst = false
        curAngle += 3.4
      curAngle += 3.4

    @usage.addChild s
    @stage.update()

  drawCirc : (graphic, angle, size, color) ->
    graphic.beginFill  color
    x = @radius + ( Math.cos(angle*(Math.PI/180)) * @radius )
    y = @radius + ( Math.sin(angle*(Math.PI/180)) * @radius )
    graphic.drawCircle x, y, size
    graphic.endFill()

pxguage.Arch = Arch
