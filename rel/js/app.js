jadeTemplate = {};
jadeTemplate['guage'] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (title, id) {
buf.push("<div class=\"guage\"><div class=\"title\">" + (jade.escape((jade_interp = title) == null ? '' : jade_interp)) + "</div><div class=\"arch\"><canvas" + (jade.attr("id", "" + (id) + "", true, false)) + " width=\"155\" height=\"78\"></canvas><div class=\"numbers\"><div class=\"used\"></div><div class=\"alloc\"></div></div></div><div class=\"sub-items\"></div></div>");}.call(this,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined,"id" in locals_for_with?locals_for_with.id:typeof id!=="undefined"?id:undefined));;return buf.join("");
};

jadeTemplate['sub-items'] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (subItems, Math) {
// iterate subItems
;(function(){
  var $$obj = subItems;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var subItem = $$obj[$index];

buf.push("<div" + (jade.attr("style", "color:" + (subItem.color) + "", true, false)) + " class=\"sub-item\"><div class=\"perc\">" + (jade.escape(null == (jade_interp = Math.round(subItem.val*100)) ? "" : jade_interp)) + "</div><div class=\"name\">" + (jade.escape(null == (jade_interp = subItem.name) ? "" : jade_interp)) + "</div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var subItem = $$obj[$index];

buf.push("<div" + (jade.attr("style", "color:" + (subItem.color) + "", true, false)) + " class=\"sub-item\"><div class=\"perc\">" + (jade.escape(null == (jade_interp = Math.round(subItem.val*100)) ? "" : jade_interp)) + "</div><div class=\"name\">" + (jade.escape(null == (jade_interp = subItem.name) ? "" : jade_interp)) + "</div></div>");
    }

  }
}).call(this);
}.call(this,"subItems" in locals_for_with?locals_for_with.subItems:typeof subItems!=="undefined"?subItems:undefined,"Math" in locals_for_with?locals_for_with.Math:typeof Math!=="undefined"?Math:undefined));;return buf.join("");
};

var SpeedGuage, pxguage;

SpeedGuage = (function() {
  SpeedGuage.count = 0;

  function SpeedGuage($el, resourceKind, kind) {
    this.kind = kind;
    this.id = "guage" + (SpeedGuage.count++);
    this.build($el, resourceKind);
  }

  SpeedGuage.prototype.setThresholds = function(warmBorder, hotBorder) {
    if (warmBorder == null) {
      warmBorder = 0.75;
    }
    if (hotBorder == null) {
      hotBorder = 0.85;
    }
    this.setUsageThresholds(warmBorder, hotBorder);
    return this.setAllocThresholds(warmBorder, hotBorder);
  };

  SpeedGuage.prototype.setUsageThresholds = function(warmUsageBorder, hotUsageBorder) {
    this.warmUsageBorder = warmUsageBorder != null ? warmUsageBorder : 0.75;
    this.hotUsageBorder = hotUsageBorder != null ? hotUsageBorder : 0.85;
  };

  SpeedGuage.prototype.setAllocThresholds = function(warmAllocBorder, hotAllocBorder) {
    this.warmAllocBorder = warmAllocBorder != null ? warmAllocBorder : 0.75;
    this.hotAllocBorder = hotAllocBorder != null ? hotAllocBorder : 0.85;
  };

  SpeedGuage.prototype.setAlloc = function(perc) {
    var $alloc, temperature;
    temperature = this.getTemperature(perc, "Alloc");
    $alloc = $(".alloc", this.$node);
    $alloc.attr("class", "alloc " + temperature);
    $alloc.html(Math.round(100 * perc));
    return this.arch.drawAlloc(perc, temperature);
  };

  SpeedGuage.prototype.setUsage = function(perc) {
    var temperature;
    temperature = this.getTemperature(perc, "Usage");
    this.displayGlobalUsage(perc, temperature);
    return this.arch.drawSingleUsage(perc, temperature);
  };


  /*
  @subUsageAr : Array of object in the following format -> {name:"Kernel", usage:"24"}
   */

  SpeedGuage.prototype.setSubUsage = function(subUsageAr) {
    var $subItems, colors, i, ind, item, node, totalUsage, _i, _j, _len, _len1;
    colors = pxguage.Colors.getSubColors();
    ind = 0;
    for (_i = 0, _len = subUsageAr.length; _i < _len; _i++) {
      item = subUsageAr[_i];
      item.color = colors[ind++];
      if (ind === colors.length) {
        ind = 0;
      }
    }
    node = jadeTemplate['sub-items']({
      subItems: subUsageAr
    });
    $subItems = $(".sub-items", this.$node);
    $subItems.append($(node));
    totalUsage = 0;
    for (i = _j = 0, _len1 = subUsageAr.length; _j < _len1; i = ++_j) {
      item = subUsageAr[i];
      totalUsage += item.val;
    }
    this.displayGlobalUsage(totalUsage, this.getTemperature(totalUsage, "Usage"));
    return this.arch.drawMultiUsage(subUsageAr);
  };

  SpeedGuage.prototype.build = function($el, resourceKind) {
    var node;
    node = jadeTemplate['guage']({
      title: resourceKind,
      id: this.id
    });
    this.$node = $(node);
    $el.append(this.$node);
    return this.arch = new pxguage.Arch(this.id);
  };

  SpeedGuage.prototype.displayGlobalUsage = function(perc, temperature) {
    var $used;
    $used = $(".used", this.$node);
    $used.attr("class", "used " + temperature);
    return $used.html(Math.round(100 * perc));
  };

  SpeedGuage.prototype.getTemperature = function(perc, kind) {
    if (perc < this["warm" + kind + "Border"]) {
      return "cool";
    } else if (perc < this["hot" + kind + "Border"]) {
      return "warm";
    } else {
      return "hot";
    }
  };

  return SpeedGuage;

})();

pxguage = {};

pxguage.SpeedGuage = SpeedGuage;

var Arch;

Arch = (function() {
  function Arch(id) {
    this.radius = 67;
    this.build(id);
  }

  Arch.prototype.build = function(id) {
    this.stage = new createjs.Stage(id);
    return this.stage.x = this.stage.y = 12;
  };

  Arch.prototype.drawAlloc = function(perc, temperature) {
    var archColor, bgArchColor, endingAngle, g, strokeColor, _ref, _ref1, _ref2;
    if (temperature === "cool") {
      _ref = pxguage.Colors.getArchColors(), bgArchColor = _ref[0], archColor = _ref[1], strokeColor = _ref[2];
    } else if (temperature === "warm") {
      _ref1 = pxguage.Colors.getWarmArchColors(), bgArchColor = _ref1[0], archColor = _ref1[1], strokeColor = _ref1[2];
    } else {
      _ref2 = pxguage.Colors.getHotArchColors(), bgArchColor = _ref2[0], archColor = _ref2[1], strokeColor = _ref2[2];
    }
    if (this.alloc != null) {
      this.stage.removeChild(this.alloc);
    }
    g = new createjs.Graphics();
    this.alloc = new createjs.Shape(g);
    this.alloc.x = this.radius;
    this.alloc.y = this.radius;
    endingAngle = (180 - 180 * perc) * -0.0174532925;
    g.setStrokeStyle(16);
    g.beginStroke(bgArchColor);
    g.arc(0, 0, this.radius, 180 * 0.0174532925, 0);
    g.endStroke();
    g.beginStroke(archColor);
    g.arc(0, 0, this.radius, 180 * 0.0174532925, endingAngle);
    g.endStroke();
    g.setStrokeStyle(3.5);
    g.beginStroke(strokeColor);
    g.arc(0, 0, this.radius + 8, 180 * 0.0174532925, endingAngle);
    g.endStroke();
    g.beginStroke(strokeColor);
    g.arc(0, 0, this.radius - 8, 180 * 0.0174532925, endingAngle);
    this.stage.addChild(this.alloc);
    return this.stage.update();
  };

  Arch.prototype.drawSingleUsage = function(perc, temperature) {
    var color, curAngle, g, s, targAngle, x1, y1;
    if (temperature === "cool") {
      color = "white";
    } else if (temperature === "warm") {
      color = pxguage.Colors.warmColor;
    } else {
      color = pxguage.Colors.hotColor;
    }
    if (this.usage != null) {
      this.stage.removeChild(this.usage);
    }
    this.usage = new createjs.Container();
    this.stage.addChild(this.usage);
    targAngle = -180 + 180 * perc;
    curAngle = -180;
    while (curAngle < targAngle) {
      curAngle += 3.4;
      x1 = this.radius + (Math.cos(curAngle * (Math.PI / 180)) * this.radius);
      y1 = this.radius + (Math.sin(curAngle * (Math.PI / 180)) * this.radius);
      g = new createjs.Graphics();
      g.beginFill(color);
      g.drawCircle(x1, y1, 1.25);
      s = new createjs.Shape(g);
      this.usage.addChild(s);
    }
    return this.stage.update();
  };

  Arch.prototype.drawMultiUsage = function(ar) {
    var curAngle, g, isFirst, item, s, targAngle, _i, _len;
    if (this.usage != null) {
      this.stage.removeChild(this.usage);
    }
    this.usage = new createjs.Container();
    this.stage.addChild(this.usage);
    curAngle = -180;
    g = new createjs.Graphics();
    s = new createjs.Shape(g);
    for (_i = 0, _len = ar.length; _i < _len; _i++) {
      item = ar[_i];
      targAngle = curAngle + 180 * item.val;
      while (curAngle < targAngle - 3.4) {
        this.drawCirc(g, curAngle, 1.25, item.color);
        isFirst = false;
        curAngle += 3.4;
      }
      curAngle += 3.4;
    }
    this.usage.addChild(s);
    return this.stage.update();
  };

  Arch.prototype.drawCirc = function(graphic, angle, size, color) {
    var x, y;
    graphic.beginFill(color);
    x = this.radius + (Math.cos(angle * (Math.PI / 180)) * this.radius);
    y = this.radius + (Math.sin(angle * (Math.PI / 180)) * this.radius);
    graphic.drawCircle(x, y, size);
    return graphic.endFill();
  };

  return Arch;

})();

pxguage.Arch = Arch;

var Colors;

Colors = (function() {
  function Colors() {}

  Colors.archBg = "#131313";

  Colors.warmColor = "#FBB869";

  Colors.hotColor = "#C12026";

  Colors.getArchColors = function() {
    return [this.archBg, "#000", "#2A3D54"];
  };

  Colors.getWarmArchColors = function() {
    return [this.archBg, "#301C0F", this.warmColor];
  };

  Colors.getHotArchColors = function() {
    return [this.archBg, "#351818", this.hotColor];
  };

  Colors.getSubColors = function() {
    return ["#DE4775", "#7FC34A", "#F9BE85", "#85D3E6"];
  };

  return Colors;

})();

pxguage.Colors = Colors;

var SubItemsBreakdown;

SubItemsBreakdown = (function() {
  function SubItemsBreakdown() {}

  return SubItemsBreakdown;

})();

pxguage.SubItemsBreakdown = SubItemsBreakdown;

var testGuage;

testGuage = new pxguage.SpeedGuage($('body'), "RAM", "dedicated");

testGuage.setThresholds(0.7, 0.8);

testGuage.setAlloc(.69);

testGuage.setUsage(.95);

testGuage.setUsage(.15);

testGuage.setSubUsage([
  {
    name: "kernel",
    val: .25
  }, {
    name: "ZFS",
    val: .2
  }, {
    name: "Global Zones",
    val: .15
  }, {
    name: "User Zones",
    val: .05
  }
]);
