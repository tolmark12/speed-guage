testGuage = new pxguage.SpeedGuage( $('body'), "RAM", "dedicated" )
testGuage.setThresholds 0.7, 0.8
testGuage.setAlloc .69
testGuage.setUsage .95
testGuage.setUsage .15
testGuage.setSubUsage [
  {name:"kernel",       val:.25}
  {name:"ZFS",          val:.2 }
  {name:"Global Zones", val:.15}
  {name:"User Zones",   val:.05}
]
